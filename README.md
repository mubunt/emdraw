 # *emdraw*,draw of a Euro-million or Loto grid.


This utility allows you to define grids for the Euro-million or the Loto by a pseudo-random draw while favoring, on demand, the numbers which have been the most or the least out. The statistics of draws are extracted in real time from referral web pages, that are :
  - https://www.secretsdujeu.com/euromillion/statistique,
  - https://www.secretsdujeu.com/euromillion/statistique-etoiles,
  - https://www.secretsdujeu.com/page/jeux_loto_statistiques.html,
  - https://www.secretsdujeu.com/loto/statistique-chance.
In previous versions (release 1.n), the statisitics were to be updated manuammy in the file *src/statistics.h*. The file, even if it is always present in the distribution, is no more used.

## LICENSE
**emdraw** is covered by the GNU General Public License (GPL) version 3 and above.

Icon from *EuroMillions Lucky Numbers* for Android (https://apkpure.com/euromillions-lucky-numbers/com.impera.euromillions).

## USAGE
``` bash
$ emdraw --help
Usage: emdraw [OPTION]...
Draw of a Euro-million or Loto grid

  -h, --help              Print help and exit
  -V, --version           Print version and exit
      --loto              Loto  (default=off)
  -m, --most              Draw by promoting the most outgoing numbers
                            (default=off)
  -l, --least             Draw by promoting the least outgoing numbers
                            (default=off)
      --nobeautification  Outputs without beautification  (default=off)
  -d, --data              Print data before drawing  (default=off)

Exit: returns a non-zero status if an error is detected.

$ emdraw -V
emdraw - Copyright (c) 2018-2020, Michel RIZZO. All Rights Reserved.
emdraw - Version 2.0.0
$ emdraw

EURO-MILLION: 50 numbers and 12 stars:  12  45  21  20  43  /  7  5

$ emdraw -m
Updating Euro-Million statistics (numbers)...... GOT... DONE
Updating Euro-Million statistics (stars)...... GOT... DONE
Most outgoing numbers:  23  44  50  19  15  /  2  3

EURO-MILLION: 50 numbers and 12 stars:  19  23  46  7  27  /  10  3

$ emdraw -l
Updating Euro-Million statistics (numbers)...... GOT... DONE
Updating Euro-Million statistics (stars)...... GOT... DONE
Least outgoing numbers:  33  46  47  22  18  /  10  1

EURO-MILLION: 50 numbers and 12 stars:  18  1  31  16  37  /  8  1

$ emdraw -l --loto
Updating Loto statistics (numbers)...... GOT... DONE
Updating Loto statistics (stars)...... GOT... DONE
Least outgoing numbers:  8  39  34  25  12  /  6

LOTO: 49 numbers and 10 stars:  7  8  20  47  25  /  8

$ emdraw -m --loto
Updating Loto statistics (numbers)...... GOT... DONE
Updating Loto statistics (stars)...... GOT... DONE
Most outgoing numbers:  41  13  22  16  1  /  7

LOTO: 49 numbers and 10 stars:  8  1  45  35  46  /  7

$ emdraw  --loto

LOTO: 49 numbers and 10 stars:  42  18  41  23  13  /  3

$ 
```

## STRUCTURE OF THE APPLICATION
This section walks you through **emdraw**'s structure. Once you understand this structure, you will easily find your way around in **emdraw**'s code base.

``` bash
$ yaTree
./                   # Application level
├── applet/          # Applet directory
│   ├── Makefile     # Makefile
│   ├── emdraw.jpeg  # Icon for applet
│   └── emdraw.py    # Applet python source
├── src/             # Source directory
│   ├── Makefile     # Makefile
│   ├── emdraw.c     # Main program
│   ├── emdraw.ggo   # 'gengetopt' option definition. Refer to https://www.gnu.org/software/gengetopt/gengetopt.html
│   └── statistics.h # Statistics for Euro-millions provided by La Française des Jeux
├── COPYING.md       # GNU General Public License markdown file
├── LICENSE.md       # License markdown file
├── Makefile         # Makefile
├── README.md        # ReadMe markdown file
├── RELEASENOTES.md  # Release Notes markdown file
└── VERSION          # Version identification text file

2 directories, 13 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd emdraw
$ make clean all
```

## HOW TO INSTALL THIS APPLICATION
```Shell
$ cd emdraw
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
```
## HOW TO USE THIS APPLICATION
Two ways for that: in a terminal or via an applet.

### In a terminal

```Shell
$ emdraw --loto

LOTO: 49 numbers and 10 lucky numbers:  27  40  17  8  9  /  8

$ 
```
### Via an applet

```Shell
$ emdraw.py &
```
Then click on the icon in the tool bar and start to play....

**IMPORTANT NOTE** With XUBUNTU 20.10, this applet does not run any more. It is no more installed.

## NOTES
- On user's request, the 5 numbers that are the most or least released during previous draws can be preferred. For this, during the pseudo-random draw, their occurrence is duplicated 10 times (4 times for "stars"). Thus, if in normal times each number has a probability of 1/50 (0.02) to be drawn, the probabilities become 1/95 for a standard number and 10/95 (0.10) for a privileged number. For "stars", the base probability of 1/12 (0.8) becomes 1/18 (0.05) and 4/18 (0.22).

## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- For development:
   - *GengetOpt* binary package installed, version 2.22.6.
   - *libsodium* library installed (/usr/locallib and /usr/local/include), version 1.0.18. Refer to https://github.com/jedisct1/libsodium.
- Developped and tested on XUBUNTU 20.10, GCC v10.2.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***