# RELEASE NOTES: *emdraw*, draw of a Euro-million or Loto grid.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 2.0.0**:
  - Draw statistics are extracted in real time from referral web pages. However, the static version is still available by compiling without the "-DAUTOSTAT" option.

- **Version 1.1.5**:
  - Updated statistic figures from a new site "https://www.secretsdujeu.com/" (15/11/2020).
  - Used these figures for loto statistics also.
  - Python applet does not run anymore with *gir1.2-appindicator3-0.1* on Ubuntu 20.10! Do not use it anaymore. Kepp sources for history....

- **Version 1.1.4**:
  - Updated build system components.

- **Version 1.1.3**:
  - Updated build system.

- **Version 1.1.2**:
  - Removed unused files.

- **Version 1.1.1**:
  - Updated build system component(s)

- **Version 1.1.0**:
  - Used libsodium's randombytes API (https://github.com/jedisct1/libsodium) instead of srand()/rand() functions.

- **Version 1.0.13**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Abandonned Windows support.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.0.12**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.0.11**:
  - Improved ./Makefile and ./src/Makefile.

- **Version 1.0.10**:
  - Updated from 29/10/2019.
  - Added option "--data/-d" to print data before drawing.
  - Fixed ./Makefile and ./src/Makefiles.

- **Version 1.0.9**:
  - Updated from 04/06/2019.

- **Version 1.0.8**:
  - Updated from 15/01/2019.

- **Version 1.0.7**:
  - Updated from 13/11/2018.

- **Version 1.0.6**:
  - Change of source after deletion of these data on the site of fdj. Source is now: https://www.lesbonsnumeros.com/euromillions/statistiques/numeros/nombre-sorties.htm. It gives numbers of output sduring the last 50 draws.
  - Updated from 02/11/2018.

- **Version 1.0.5**:
  - Updated the euro-million statistics with the numbers provided by FDJ from 05/10/2018.

- **Version 1.0.4**:
  - Updated the euro-million statistics with the numbers provided by FDJ from 21/09/2018.

*- *Version 1.0.3**:
  - Updated the euro-million statistics with the numbers provided by FDJ from 11/09/2018.

- **Version 1.0.2**:
  - Fixed the omission to add the path for the lotto draw executable.
  - Improved "A Propos" text.
  - Moded to Python 3.

- **Version 1.0.1**:
  - Fixed url of *Française des Jeux* in *README.md* file.
  - Added */gitignore* file.
  - Some writing improvements in *emdraw.py*.
  - Added the purpose of the application in output of *--version* option of *emdraw*.

- **Version 1.0.0**:
  - First version.
