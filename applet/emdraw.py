#!/usr/bin/env python3
#------------------------------------------------------------------------------
# Copyright (c) 2018, Michel RIZZO.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: emdraw
# Draw of a Euro-million or Loto grid.
#-------------------------------------------------------------------------------

# This code was developed from the example given in the excellent Tim's tutorial:
# http://candidtim.github.io/appindicator/2014/09/13/ubuntu-appindicator-step-by-step.html
import os
import signal
import gi
import subprocess
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')
gi.require_version('Notify', '0.7')
from gi.repository import Gtk as gtk
from gi.repository import AppIndicator3 as appindicator
from gi.repository import Notify as notify
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
APPINDICATOR_ID = 'emdraw_appindicator'
#EMDRAW = os.path.dirname(os.path.abspath(__file__)) + "/emdraw"
#ICON_FILE = os.path.dirname(os.path.abspath(__file__)) + "/emdraw.jpeg"
EMDRAW = "../linux/emdraw"
ICON_FILE = "/.emdraw.jpeg"
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
def main():
	indicator = appindicator.Indicator.new(APPINDICATOR_ID, ICON_FILE, appindicator.IndicatorCategory.SYSTEM_SERVICES)
	indicator.set_status(appindicator.IndicatorStatus.ACTIVE)
	indicator.set_menu(build_menu())
	notify.init(APPINDICATOR_ID)
	gtk.main()
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
def build_menu():
	menu = gtk.Menu()

	item_em = gtk.MenuItem(label="Euro-millions")
	item_em.connect('activate', em)
	menu.append(item_em)

	item_emmost = gtk.MenuItem(label="Euro-millions / Numeros les + sortis")
	item_emmost.connect('activate', emmost)
	menu.append(item_emmost)

	item_emleast = gtk.MenuItem(label="Euro-millions / Numeros les - sortis")
	item_emleast.connect('activate', emleast)
	menu.append(item_emleast)

	menu.append(gtk.SeparatorMenuItem())

	item_loto = gtk.MenuItem(label="Loto")
	item_loto.connect('activate', lt)
	menu.append(item_loto)

	item_ltmost = gtk.MenuItem(label="Loto / Numeros les + sortis")
	item_ltmost.connect('activate', ltmost)
	menu.append(item_ltmost)

	item_ltleast = gtk.MenuItem(label="Loto / Numeros les - sortis")
	item_ltleast.connect('activate', ltleast)
	menu.append(item_ltleast)

	menu.append(gtk.SeparatorMenuItem())

	item_about = gtk.MenuItem(label="A propos de ...")
	item_about.connect('activate', about)
	menu.append(item_about)

	item_quit = gtk.MenuItem(label="Quitter")
	item_quit.connect('activate', quit)
	menu.append(item_quit)

	menu.show_all()
	return menu
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
def em(_):
	output = subprocess.check_output([EMDRAW, "--nobeautification"]).decode("utf-8")
	note = notify.Notification.new("EURO-MILLIONS", output, None)
	note.set_urgency(2)
	note.show()
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
def emmost(_):
	output = subprocess.check_output([EMDRAW, "--nobeautification", "--most"]).decode("utf-8")
	note = notify.Notification.new("EURO-MILLIONS - Most outgoing numbers", output, None)
	note.set_urgency(2)
	note.show()
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
def emleast(_):
	output = subprocess.check_output([EMDRAW, "--nobeautification", "--least"]).decode("utf-8")
	note = notify.Notification.new("EURO-MILLIONS - Least outgoing numbers", output, None)
	note.set_urgency(2)
	note.show()
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
def lt(_):
	output = subprocess.check_output([EMDRAW, "--nobeautification", "--loto"]).decode("utf-8")
	note = notify.Notification.new("LOTO", output, None)
	note.set_urgency(2)
	note.show()
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
def ltmost(_):
	output = subprocess.check_output([EMDRAW, "--nobeautification", "--loto", "--most"]).decode("utf-8")
	note = notify.Notification.new("LOTO - Most outgoing numbers", output, None)
	note.set_urgency(2)
	note.show()
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
def ltleast(_):
	output = subprocess.check_output([EMDRAW, "--nobeautification", "--loto", "--least"]).decode("utf-8")
	note = notify.Notification.new("LOTO - Least outgoing numbers", output, None)
	note.set_urgency(2)
	note.show()
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
def about(_):
	output = subprocess.check_output([EMDRAW, "--version"]).decode("utf-8")
	note = notify.Notification.new("A PROPOS DE ...\n", output, None)
	note.set_urgency(1)
	note.show()
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
def quit(_):
	notify.uninit()
	gtk.main_quit()
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
if __name__ == "__main__":
	signal.signal(signal.SIGINT, signal.SIG_DFL)
	main()
#-------------------------------------------------------------------------------
