//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emdraw
// Draw of a Euro-million or Loto grid.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#include <sodium.h>

#ifdef AUTOSTAT
#include <curl/curl.h>
#endif
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emdraw_cmdline.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define ERROR(fmt, ...)     			fprintf(stderr,"\nERROR: " fmt ". Abort!\n", __VA_ARGS__)

#define EM_NUMBER_OF_BALLS				50		// Euro-Millions
#define EM_NUMBER_OF_STARS				12		// Euro-Millions
#define EM_NUMBER_OF_BALLS_TO_DRAW		5		// Euro-Millions
#define EM_NUMBER_OF_STARS_TO_DRAW		2		// Euro-Millions

#define LT_NUMBER_OF_BALLS				49		// Loto
#define LT_NUMBER_OF_STARS				10		// Loto
#define LT_NUMBER_OF_BALLS_TO_DRAW		5		// Loto
#define LT_NUMBER_OF_STARS_TO_DRAW		1		// Loto

#define WEIGHT_TO_PROMOTE_A_BALL		10		// 
#define WEIGHT_TO_PROMOTE_A_STAR		4		//

#ifdef AUTOSTAT
#define TMP_TEMPLATE					"/tmp/emdrawTemporaryXXXXXX"
#define TMPSIZE							128
#endif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//-- Escaped chars
#define ESC_ATTRIBUTSOFF				"\033[0m"
#define ESC_STRONG_ON					"\033[1m"
#define ESC_EMPHASIS_ON					"\033[3m"
#define ESC_REVERSE_ON					"\033[7m"
#define ESC_STRIKE_ON					"\033[9m"
#define ESC_COLOR						"\033[%dm"
//-- Color management
#define FOREGROUND(color) 				(30 + color)
#define BACKGROUND(color) 				(40 + color)
//-- Macro code: Display
//------------------------------------------------------------------------------
// TYPDEFS
//------------------------------------------------------------------------------
typedef enum { NORMAL = 0, EMMOST, EMLEAST, LTMOST, LTLEAST } drawtype;
typedef enum { black = 0, red, green, yellow, blue, magenta, cyan, white } e_color;
#ifdef AUTOSTAT
typedef enum { EUROMILLION = 0, LOTO = 2 } gametype;
#endif
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct {
	int		number;
	bool 	drawed;
} s_balls;

#ifdef AUTOSTAT
typedef struct {
	int number;
	int times;
} ballstat;
#endif
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static s_balls		*all_balls					= NULL;
static s_balls		*all_stars					= NULL;
static int			*drawed_balls				= NULL;
static int			*drawed_stars				= NULL;
static int			number_of_balls 			= 0;
static int			number_of_stars 			= 0;
static int			number_of_balls_to_draw 	= 0;
static int			number_of_stars_to_draw 	= 0;

#ifdef AUTOSTAT
static ballstat		em_statistic_balls[EM_NUMBER_OF_BALLS];
static ballstat		em_statistic_stars[EM_NUMBER_OF_STARS];
static ballstat		lt_statistic_balls[LT_NUMBER_OF_BALLS];
static ballstat		lt_statistic_stars[LT_NUMBER_OF_STARS];

static int 			em_statistic_most_balls[5];
static int 			em_statistic_most_stars[2];
static int 			em_statistic_least_balls[5];
static int 			em_statistic_least_stars[2];

static int 			lt_statistic_most_balls[5];
static int 			lt_statistic_most_stars[1];
static int 			lt_statistic_least_balls[5];
static int 			lt_statistic_least_stars[1];

static FILE *fptr;
static const char urls[4][80] = {
	"https://www.secretsdujeu.com/euromillion/statistique",
	"https://www.secretsdujeu.com/euromillion/statistique-etoiles",
	"https://www.secretsdujeu.com/page/jeux_loto_statistiques.html",
	"https://www.secretsdujeu.com/loto/statistique-chance"
};
static const char actions[4][80] = {
	"Updating Euro-Million statistics (numbers)...",
	"Updating Euro-Million statistics (stars)...",
	"Updating Loto statistics (numbers)...",
	"Updating Loto statistics (stars)..."
};
#else
#include "statistics.h"
#endif
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void printStat( drawtype type, const char *title, const int *tabb, const int *tabs ) {
	int k1 = (type == EMMOST || type == EMLEAST) ? EM_NUMBER_OF_BALLS_TO_DRAW : LT_NUMBER_OF_BALLS_TO_DRAW;
	int k2 = (type == EMMOST || type == EMLEAST) ? EM_NUMBER_OF_STARS_TO_DRAW : LT_NUMBER_OF_STARS_TO_DRAW;
	int i;
	fprintf(stdout, "%s", title);
	for (i = 0; i < k1; i++)
		fprintf(stdout, "  %d", tabb[i]);
	fprintf(stdout, "  /");
	for (i = 0; i < k2; i++) {
		fprintf(stdout, "  %d", tabs[i]);
	}
	fprintf(stdout, "\n");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void printData( void ) {
	int i;
	fprintf(stdout, "BALLS:");
	for (i = 0; i < number_of_balls; i++) {
		fprintf(stdout, "  %d", all_balls[i].number);
	}
	fprintf(stdout, "\nSTARS:");
	for (i = 0; i < number_of_stars; i++) {
		fprintf(stdout, "  %d", all_stars[i].number);
	}
	fprintf(stdout, "\n\n");
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#ifdef AUTOSTAT
static void printData2( gametype game ) {
	fprintf(stdout, "STATISTICS / BALLS:\n");
	switch (game) {
	case EUROMILLION:
		for (int j = 0; j < EM_NUMBER_OF_BALLS; j++) {
			fprintf(stdout, "\t%2d (x%d)", em_statistic_balls[j].number, em_statistic_balls[j].times);
			if (0 == ((j + 1) % 4)) fprintf(stdout, "\n");
		}
		break;
	case LOTO:
		for (int j = 0; j < LT_NUMBER_OF_BALLS; j++) {
			fprintf(stdout, "\t%2d (x%d)", lt_statistic_balls[j].number, lt_statistic_balls[j].times);
			if (0 == ((j + 1) % 4)) fprintf(stdout, "\n");
		}
		break;
	}
	fprintf(stdout, "\nSTATISTICS / STARS:\n");
	switch (game) {
	case EUROMILLION:
		for (int j = 0; j < EM_NUMBER_OF_STARS; j++) {
			fprintf(stdout, "\t%2d (x%d)", em_statistic_stars[j].number, em_statistic_stars[j].times);
			if (0 == ((j + 1) % 4)) fprintf(stdout, "\n");
		}
		break;
	case LOTO:
		for (int j = 0; j < LT_NUMBER_OF_STARS; j++) {
			fprintf(stdout, "\t%2d (x%d)", lt_statistic_stars[j].number, lt_statistic_stars[j].times);
			if (0 == ((j + 1) % 4)) fprintf(stdout, "\n");
		}
		break;
	}
	fprintf(stdout, "\n");
}
#endif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static int randomDraw( int max ) {
	return (int) randombytes_uniform((uint32_t)max);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void initMostOrLeast( s_balls *tab, int max, int maxgame, const int *stat, int nbstat, int weight ) {
	int i, j, n;
	for (i = 0; i < max; i++)
		tab[i].drawed = false;
	for (i = 0; i < nbstat; i++) {
		for (j = 0; j < weight - 1; j++) {
			do n = randomDraw(maxgame - 1);
			while (tab[n].drawed);
			tab[n].drawed = true;
			tab[n].number = stat[i];
		}
	}
	j = 0;
	for (i = 0; i < maxgame; i++) {
		while (tab[j].drawed) {
			tab[j].drawed = false;
			++j;
		}
		tab[j].number = i + 1;
		++j;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static void initDrawData( drawtype type ) {
	all_balls = calloc((size_t) number_of_balls, sizeof(s_balls));
	all_stars = calloc((size_t) number_of_stars, sizeof(s_balls));
	drawed_balls = calloc((size_t) number_of_balls_to_draw, sizeof(int));
	drawed_stars = calloc((size_t) number_of_stars_to_draw, sizeof(int));
	int i, j, n;
	switch (type) {
	case NORMAL:
		for (i = 0; i < number_of_balls; i++) {
			all_balls[i].number = i + 1;
			all_balls[i].drawed = false;
		}
		for (i = 0; i < number_of_stars; i++) {
			all_stars[i].number = i + 1;
			all_stars[i].drawed = false;
		}
		break;
	case EMMOST:
		initMostOrLeast(all_balls, number_of_balls, EM_NUMBER_OF_BALLS,
		                em_statistic_most_balls, (int) (sizeof(em_statistic_most_balls) / sizeof(int)), WEIGHT_TO_PROMOTE_A_BALL);
		initMostOrLeast(all_stars, number_of_stars, EM_NUMBER_OF_STARS,
		                em_statistic_most_stars, (int) (sizeof(em_statistic_most_stars) / sizeof(int)), WEIGHT_TO_PROMOTE_A_STAR);
		break;
	case EMLEAST:
		initMostOrLeast(all_balls, number_of_balls, EM_NUMBER_OF_BALLS,
		                em_statistic_least_balls, (int) (sizeof(em_statistic_least_balls) / sizeof(int)), WEIGHT_TO_PROMOTE_A_BALL);
		initMostOrLeast(all_stars, number_of_stars, EM_NUMBER_OF_STARS,
		                em_statistic_least_stars, (int) (sizeof(em_statistic_least_stars) / sizeof(int)), WEIGHT_TO_PROMOTE_A_STAR);
		break;
	case LTMOST:
		initMostOrLeast(all_balls, number_of_balls, LT_NUMBER_OF_BALLS,
		                lt_statistic_most_balls, (int) (sizeof(lt_statistic_most_balls) / sizeof(int)), WEIGHT_TO_PROMOTE_A_BALL);
		initMostOrLeast(all_stars, number_of_stars, LT_NUMBER_OF_STARS,
		                lt_statistic_most_stars, (int) (sizeof(lt_statistic_most_stars) / sizeof(int)), WEIGHT_TO_PROMOTE_A_STAR);
		break;
	case LTLEAST:
		initMostOrLeast(all_balls, number_of_balls, LT_NUMBER_OF_BALLS,
		                lt_statistic_least_balls, (int) (sizeof(lt_statistic_least_balls) / sizeof(int)), WEIGHT_TO_PROMOTE_A_BALL);
		initMostOrLeast(all_stars, number_of_stars, LT_NUMBER_OF_STARS,
		                lt_statistic_least_stars, (int) (sizeof(lt_statistic_least_stars) / sizeof(int)), WEIGHT_TO_PROMOTE_A_STAR);
		break;
	}
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#ifdef AUTOSTAT
static char *gettemporaryfilename( char *name ) {
	int fd;
	strcpy(name, TMP_TEMPLATE);
	if (-1 == (fd = mkstemp(name))) return NULL;
	close(fd);
	return name;
}
#endif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#ifdef AUTOSTAT
static void unlinktemporaryfile( char *name ) {
	unlink(name);
}
#endif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#ifdef AUTOSTAT
static size_t write_callback(void *ptr, size_t size, size_t nmemb, void *stream) {
	return fwrite(ptr, size, nmemb, fptr);
}
#endif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#ifdef AUTOSTAT
static int getfigures( unsigned int index, char *filename ) {
	FILE *fd;
	if (NULL == (fd = fopen(filename, "r"))) {
		ERROR("%s", "Cannot open temporary file");
		return 1;
	}
	fseek(fd, 0, SEEK_END);
	size_t length = (size_t) ftell(fd) + 1;
	fseek(fd, 0L, SEEK_SET);
	char *buffer = malloc(length * sizeof(char));
	if (buffer == NULL) {
		ERROR("%s", "Cannot allocate memory to read html page");
		return 1;
	}
	if (NULL == fgets(buffer, (int) length, fd)) {
		ERROR("%s", "Cannot read html page");
		return 1;
	}
	char *pt;
	int i, res = 0;
	if (NULL == (pt = strstr(buffer, "<tbody><tr class=odd><td class=\"title bold\">"))) {
		ERROR("%s", "Cannot find sart of information in html page");
		res = 1;
	} else {
		switch (index) {
		case 0:
			for (int i = 0; i < EM_NUMBER_OF_BALLS; i++) {
				if (0 == (i % 2)) {
					pt = strstr(pt, "<tr class=odd>");
					sscanf(pt, "<tr class=odd><td class=\"title bold\">%d</td><td>%d</td>", &em_statistic_balls[i].number, &em_statistic_balls[i].times);
				} else {
					pt = strstr(pt, "<tr class=even>");
					sscanf(pt, "<tr class=even><td class=\"title bold\">%d</td><td>%d</td>", &em_statistic_balls[i].number, &em_statistic_balls[i].times);
				}
			}
			break;
		case 1:
			for (int i = 0; i < EM_NUMBER_OF_STARS; i++) {
				if (0 == (i % 2)) {
					pt = strstr(pt, "<tr class=odd>");
					sscanf(pt, "<tr class=odd><td class=\"title bold\">%d</td><td>%d</td>", &em_statistic_stars[i].number, &em_statistic_stars[i].times);
				} else {
					pt = strstr(pt, "<tr class=even>");
					sscanf(pt, "<tr class=even><td class=\"title bold\">%d</td><td>%d</td>", &em_statistic_stars[i].number, &em_statistic_stars[i].times);
				}
			}
			break;
		case 2:
			for (int i = 0; i < LT_NUMBER_OF_BALLS; i++) {
				if (0 == (i % 2)) {
					pt = strstr(pt, "<tr class=odd>");
					sscanf(pt, "<tr class=odd><td class=\"title bold\">%d</td><td>%d</td>", &lt_statistic_balls[i].number, &lt_statistic_balls[i].times);
				} else {
					pt = strstr(pt, "<tr class=even>");
					sscanf(pt, "<tr class=even><td class=\"title bold\">%d</td><td>%d</td>", &lt_statistic_balls[i].number, &lt_statistic_balls[i].times);
				}
			}
			break;
		case 3:
			for (int i = 0; i < LT_NUMBER_OF_STARS; i++) {
				if (0 == (i % 2)) {
					pt = strstr(pt, "<tr class=odd>");
					sscanf(pt, "<tr class=odd><td class=\"title bold\">%d</td><td>%d</td>", &lt_statistic_stars[i].number, &lt_statistic_stars[i].times);
				} else {
					pt = strstr(pt, "<tr class=even>");
					sscanf(pt, "<tr class=even><td class=\"title bold\">%d</td><td>%d</td>", &lt_statistic_stars[i].number, &lt_statistic_stars[i].times);
				}
			}
			break;
		}
	}
	free(buffer);
	return res;
}
#endif
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#ifdef AUTOSTAT
static int updatefigures( gametype game ) {
	char tmpfile[TMPSIZE];
	if (NULL == gettemporaryfilename(tmpfile)) {
		ERROR("%s", "Cannot get temporary file");
		return 1;
	}
	bool error = false;
	for (unsigned int idx = game; idx <= game + 1; idx++) {
		fprintf(stdout, "%s...", actions[idx]);
		fflush(stdout);

		if (NULL == (fptr = fopen(tmpfile, "w+"))) {
			ERROR("%s", "Cannot create temporary file");
			error = true;
			break;
		}
		CURL *curl = curl_easy_init();
		if (! curl) {
			ERROR("%s", "curl_easy_init() failed");
			error = true;
			break;
		}
		curl_easy_setopt(curl, CURLOPT_URL, urls[idx]);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);
		CURLcode res = curl_easy_perform(curl);
		fclose(fptr);
		curl_easy_cleanup(curl);
		if(res != CURLE_OK) {
			ERROR("curl_easy_perform() failed: %s", curl_easy_strerror(res));
			error = true;
			break;
		}
		fprintf(stdout, " GOT...");
		fflush(stdout);
		if (0 != getfigures(idx, tmpfile)) {
			error = true;
			break;
		}
		fprintf(stdout, " DONE\n");
	}
	unlinktemporaryfile(tmpfile);
	return (int) error == true;
}
#endif
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct gengetopt_args_info args_info;
	//---- Parameter checking and setting --------------------------------------
	if (sodium_init() < 0) {
		ERROR("%s", "PANIC - the library couldn't be initialized, it is not safe to use");
		return EXIT_FAILURE;
	}
	if (cmdline_parser_emdraw(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//---- Initializations -----------------------------------------------------
	if (args_info.loto_given) {
		if (args_info.most_given && args_info.least_given)
			fprintf(stdout, "Options --most (-m) and --least (-l) are exclusive. --least is ignored.\n");
		if (args_info.most_given || args_info.least_given) {
#ifdef AUTOSTAT
			if (updatefigures(LOTO)) return EXIT_FAILURE;
			for (int i = 0; i < LT_NUMBER_OF_BALLS_TO_DRAW; i++) {
				lt_statistic_most_balls[i] = lt_statistic_balls[i].number;
				lt_statistic_least_balls[i] = lt_statistic_balls[LT_NUMBER_OF_BALLS - i - 1].number;
			}
			for (int i = 0; i < LT_NUMBER_OF_STARS_TO_DRAW; i++) {
				lt_statistic_most_stars[i] = lt_statistic_stars[i].number;
				lt_statistic_least_stars[i] = lt_statistic_stars[LT_NUMBER_OF_STARS - i - 1].number;
			}
			if (! args_info.nobeautification_given && args_info.data_given)
				printData2(LOTO);
#else
			fprintf(stdout, "These statistics were updated on %s.\n", lt_date_of_statistic_update);
#endif
			int n = (int) sizeof(lt_statistic_most_balls) / sizeof(int);
			n = n * (WEIGHT_TO_PROMOTE_A_BALL - 1);
			number_of_balls = LT_NUMBER_OF_BALLS + n;
			n = (int) sizeof(lt_statistic_most_stars) / sizeof(int);
			n = n * (WEIGHT_TO_PROMOTE_A_STAR - 1);
			number_of_stars = LT_NUMBER_OF_STARS + n;
			number_of_balls_to_draw = LT_NUMBER_OF_BALLS_TO_DRAW;
			number_of_stars_to_draw = LT_NUMBER_OF_STARS_TO_DRAW;
			if (args_info.most_given) {
				printStat(LTMOST, "Most outgoing numbers:", lt_statistic_most_balls, lt_statistic_most_stars);
				initDrawData(LTMOST);
			} else {
				printStat(LTLEAST, "Least outgoing numbers:", lt_statistic_least_balls, lt_statistic_least_stars);
				initDrawData(LTLEAST);
			}
		} else {
			number_of_balls = LT_NUMBER_OF_BALLS;
			number_of_stars = LT_NUMBER_OF_STARS;
			number_of_balls_to_draw = LT_NUMBER_OF_BALLS_TO_DRAW;
			number_of_stars_to_draw = LT_NUMBER_OF_STARS_TO_DRAW;
			initDrawData(NORMAL);
		}
		if (args_info.nobeautification_given)
			fprintf(stdout, "\nLOTO:");
		else
			fprintf(stdout, "\nLOTO: %d numbers and %d stars:", LT_NUMBER_OF_BALLS, LT_NUMBER_OF_STARS);
	} else {
		if (args_info.most_given && args_info.least_given)
			fprintf(stdout, "Options --most (-m) and --least (-l) are exclusive. --least is ignored.\n");
		if (args_info.most_given || args_info.least_given) {
#ifdef AUTOSTAT
			if (updatefigures(EUROMILLION)) return EXIT_FAILURE;
			for (int i = 0; i < EM_NUMBER_OF_BALLS_TO_DRAW; i++) {
				em_statistic_most_balls[i] = em_statistic_balls[i].number;
				em_statistic_least_balls[i] = em_statistic_balls[EM_NUMBER_OF_BALLS - i - 1].number;
			}
			for (int i = 0; i < EM_NUMBER_OF_STARS_TO_DRAW; i++) {
				em_statistic_most_stars[i] = em_statistic_stars[i].number;
				em_statistic_least_stars[i] = em_statistic_stars[EM_NUMBER_OF_STARS - i - 1].number;
			}
			if (! args_info.nobeautification_given && args_info.data_given)
				printData2(EUROMILLION);
#else
			fprintf(stdout, "These statistics were updated on %s.\n", em_date_of_statistic_update);
#endif
			int n = (int) sizeof(em_statistic_most_balls) / sizeof(int);
			n = n * (WEIGHT_TO_PROMOTE_A_BALL - 1);
			number_of_balls = EM_NUMBER_OF_BALLS + n;
			n = (int) sizeof(em_statistic_most_stars) / sizeof(int);
			n = n * (WEIGHT_TO_PROMOTE_A_STAR - 1);
			number_of_stars = EM_NUMBER_OF_STARS + n;
			number_of_balls_to_draw = EM_NUMBER_OF_BALLS_TO_DRAW;
			number_of_stars_to_draw = EM_NUMBER_OF_STARS_TO_DRAW;
			if (args_info.most_given) {
				printStat(EMMOST, "Most outgoing numbers:", em_statistic_most_balls, em_statistic_most_stars);
				initDrawData(EMMOST);
			} else {
				printStat(EMLEAST, "Least outgoing numbers:", em_statistic_least_balls, em_statistic_least_stars);
				initDrawData(EMLEAST);
			}
		} else {
			number_of_balls = EM_NUMBER_OF_BALLS;
			number_of_stars = EM_NUMBER_OF_STARS;
			number_of_balls_to_draw = EM_NUMBER_OF_BALLS_TO_DRAW;
			number_of_stars_to_draw = EM_NUMBER_OF_STARS_TO_DRAW;
			initDrawData(NORMAL);
		}
		if (args_info.nobeautification_given)
			fprintf(stdout, "\nEURO-MILLION:");
		else
			fprintf(stdout, "\nEURO-MILLION: %d numbers and %d stars:", EM_NUMBER_OF_BALLS, EM_NUMBER_OF_STARS);
	}
	//---- Draw ----------------------------------------------------------------
	int n, i, j;
	for (i = 0; i < number_of_balls_to_draw; i++) {
		do n = randomDraw(number_of_balls - 1);
		while (all_balls[n].drawed);
		all_balls[n].drawed = true;
		drawed_balls[i] = all_balls[n].number;
		for (j = 0; j < number_of_balls; j++) {
			if (all_balls[j].number == all_balls[n].number) all_balls[j].drawed = true;
		}
	}
	for (i = 0; i < number_of_stars_to_draw; i++) {
		do n = randomDraw(number_of_stars - 1);
		while (all_stars[n].drawed);
		all_stars[n].drawed = true;
		drawed_stars[i] = all_stars[n].number;
		for (j = 0; j < number_of_stars; j++) {
			if (all_stars[j].number == all_stars[n].number) all_stars[j].drawed = true;
		}
	}
	//---- Display -------------------------------------------------------------
	if (! args_info.nobeautification_given)
		fprintf(stdout, ESC_COLOR ESC_STRONG_ON, FOREGROUND(yellow));
	for (i = 0; i < number_of_balls_to_draw; i++) {
		fprintf(stdout, "  %d", drawed_balls[i]);
	}
	fprintf(stdout, "  /");
	for (i = 0; i < number_of_stars_to_draw; i++) {
		fprintf(stdout, "  %d", drawed_stars[i]);
	}
	if (! args_info.nobeautification_given) {
		fprintf(stdout, ESC_ATTRIBUTSOFF "\n\n");
		if (args_info.data_given)
			printData();
	}
	//---- Exit ----------------------------------------------------------------
	free(all_balls);
	free(all_stars);
	free(drawed_balls);
	free(drawed_stars);
	cmdline_parser_emdraw_free(&args_info);
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
