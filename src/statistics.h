//------------------------------------------------------------------------------
// Copyright (c) 2018-2019, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emdraw
// Draw of a Euro-million or Loto grid.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// EURO-MILLIONS AND LOTOS STATISTICS
//------------------------------------------------------------------------------
//Change of source after deletion of these data on the site of fdj.
//Source:https://www.secretsdujeu.com/euromillion/statistique

static const char	*em_date_of_statistic_update= "15/11/2020";
static const int 	em_statistic_most_balls[5]	= { 23, 44, 50, 19, 15 };
static const int 	em_statistic_most_stars[2]	= { 2, 3 };
static const int 	em_statistic_least_balls[5]	= { 33, 46, 47, 22, 18 };
static const int 	em_statistic_least_stars[2]	= { 10, 1 };

//Source: https://www.secretsdujeu.com/page/jeux_loto_statistiques.html
static const char	*lt_date_of_statistic_update= "15/11/2020";
static const int 	lt_statistic_most_balls[5]	= { 41, 13, 22, 16, 1 };
static const int 	lt_statistic_most_stars[1]	= { 7 };
static const int 	lt_statistic_least_balls[5]	= { 8, 39, 34, 25, 12 };
static const int 	lt_statistic_least_stars[1]	= { 6 };

//Source: https://www.lesbonsnumeros.com/euromillions/statistiques/numeros/nombre-sorties.htm
//Number of output during the last 50 draws.

//static const char	*date_of_statistic_update	= "29/10/2019";
//static const int 	statistic_most_balls[5]		= { 44, 50, 23, 19, 4 };
//static const int 	statistic_most_stars[2]		= { 2, 3 };
//static const int 	statistic_least_balls[5]	= { 46, 33, 41, 47, 22 };
//static const int 	statistic_least_stars[2]	= { 12, 11 };

//static const char	*date_of_statistic_update	= "04/06/2019";
//static const int 	statistic_most_balls[5]		= { 50, 23, 44, 4, 19 };
//static const int 	statistic_most_stars[2]		= { 2, 8 };
//static const int 	statistic_least_balls[5]	= { 46, 33, 47, 41, 22 };
//static const int 	statistic_least_stars[2]	= { 12, 11 };

//static const char	*date_of_statistic_update	= "15/01/2019";
//static const int 	statistic_most_balls[5]		= { 50, 44, 23, 4, 17 };
//static const int 	statistic_most_stars[2]		= { 12, 3 };
//static const int 	statistic_least_balls[5]	= { 46, 33, 47, 41, 2 };
//static const int 	statistic_least_stars[2]	= { 5, 7 };

//static const char	*date_of_statistic_update	= "13/11/2018";
//static const int 	statistic_most_balls[5]		= { 15, 43, 4, 23, 38 };
//static const int 	statistic_most_stars[2]		= { 12, 3 };
//static const int 	statistic_least_balls[5]	= { 10, 11, 13, 19, 22 };
//static const int 	statistic_least_stars[2]	= { 7, 10 };

//static const char	*date_of_statistic_update	= "02/11/2018";
//static const int 	statistic_most_balls[5]		= { 15, 4, 43, 23, 31 };
//static const int 	statistic_most_stars[2]		= { 14, 12 };
//static const int 	statistic_least_balls[5]	= { 10, 13, 19, 22, 34 };
//static const int 	statistic_least_stars[2]	= { 7, 10 };

//static const char	*date_of_statistic_update	= "05/10/2018";
//static const int 	statistic_most_balls[5]		= { 20, 17, 23, 48, 15 };
//static const int 	statistic_most_stars[2]		= { 3, 4 };
//static const int 	statistic_least_balls[5]	= { 47, 32, 33, 40, 18 };
//static const int 	statistic_least_stars[2]	= { 1, 7 };

//static const char	*date_of_statistic_update	= "21/09/2018";
//static const int 	statistic_most_balls[5]		= { 17, 20, 23, 48, 44 };
//static const int 	statistic_most_stars[2]		= { 3, 4 };
//static const int 	statistic_least_balls[5]	= { 40, 32, 33, 35, 18 };
//static const int 	statistic_least_stars[2]	= { 1, 7 };

//static const char	*date_of_statistic_update	= "11/09/2018";
//static const int 	statistic_most_balls[5]		= { 17, 20, 23, 48, 44 };
//static const int 	statistic_most_stars[2]		= { 3, 2 };
//static const int 	statistic_least_balls[5]	= { 35, 33, 40, 32, 18 };
//static const int 	statistic_least_stars[2]	= { 1, 7 };

//static const char	*date_of_statistic_update	= "07/09/2018";
//static const int 	statistic_most_balls[5]		= { 17, 20, 23, 48, 44 };
//static const int 	statistic_most_stars[2]		= { 9, 3 };
//static const int 	statistic_least_balls[5]	= { 35, 33, 40, 32, 18 };
//static const int 	statistic_least_stars[2]	= { 1, 7 };
//------------------------------------------------------------------------------
